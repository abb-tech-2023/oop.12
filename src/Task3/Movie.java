package src.Task3;

public class Movie extends  Media {
    public Movie(String title, int duration) {
        super(title, duration);
    }

    @Override
    public void play() {
        System.out.println("Playing movie: " + getTitle());

    }
}


