package src.Task3;

public class Music  extends  Media{
    public Music(String title, int duration) {
        super(title, duration);
    }

    @Override
    public void play() {
        System.out.println("Playing music: " + getTitle());

    }
}


