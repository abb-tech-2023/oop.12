package src.Task3;

public abstract class Media {  private String title;
    private  int duration;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public  Media(String title, int duration) {
        this.title= title;
        this. duration=duration;

    }
    public abstract void play();
}


