package src.Task2;

public class Teacher extends Person {
    private String subject;

    public Teacher (String name, int age, String subject) {
        super(name, age);
        this.subject = subject;
    }



    @Override
    public void displayInfo(String name, int age) {
        System.out.println("Teacher name "+ name);
        System.out.println("Teacher age"+ age);
        System.out.println("Subject " + subject);

    }
}

