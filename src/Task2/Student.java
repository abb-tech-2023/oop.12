package src.Task2;

public class Student extends Person {
    private int studentID;

    public Student(String name, int age, int studentID) {
        super(name, age);
        this.studentID = studentID;
    }



    @Override
    public void displayInfo(String name, int age) {
        System.out.println("student name: "+ name);
        System.out.println("student age: "+ age);
        System.out.println("student ID: "+ studentID);

    }
}

